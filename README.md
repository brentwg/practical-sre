# Practical Guide to Site Reliability Engineering

Inspired by [The Must Know Checklist for DevOps & Site Reliability Engineers (updated 2018)](https://medium.com/devopslinks/the-must-know-checklist-for-devops-site-reliability-engineers-update-8ba44dbc824), this is my personal How-To and knowledge base links repository. Why bother recording this? Because I'm getting old and my memory isn't half as good as it used to be. And you're only as good as all of the things you can quickly look up... So here's job-related infomation about almost everything I've most recently researched.

In addition, this page is an experiment to see just how large I can make a single README page until it become completely unmanageable. Lots going on...

- [Practical Guide to Site Reliability Engineering](#practical-guide-to-site-reliability-engineering)
  - [Basic Operating Systems](#basic-operating-systems)
    - [macOS](#macos)
      - [How to capture screenshot directly to clipboard](#how-to-capture-screenshot-directly-to-clipboard)
    - [Linux](#linux)
      - [All Distros](#all-distros)
        - [cron](#cron)
        - [Cpu Information](#cpu-information)
      - [Ubuntu](#ubuntu)
        - [Active Directory / LDAP / SSSD](#active-directory--ldap--sssd)
  - [Basic Storage](#basic-storage)
    - [Linux Basic Storage Commands](#linux-basic-storage-commands)
      - [Logical Volume Manager](#logical-volume-manager)
      - [Encrypted Storage](#encrypted-storage)
      - [Swap](#swap)
  - [Basic Networking](#basic-networking)
  - [Basic Monitoring](#basic-monitoring)
  - [Basic Security](#basic-security)
    - [SSH](#ssh)
  - [Cloud](#cloud)
    - [AWS](#aws)
      - [EC2](#ec2)
  - [Containers](#containers)
  - [Orchestration](#orchestration)
  - [Automation](#automation)
  - [Programming](#programming)
    - [Code Snippets](#code-snippets)
      - [VS Code](#vs-code)
    - [Python](#python)
    - [Golang](#golang)
      - [Golang: Atom Configuration](#golang-atom-configuration)
  - [Applications](#applications)
    - [OmniGraffle](#omnigraffle)
      - [How to install stencils (Mac)](#how-to-install-stencils-mac)
    - [vim](#vim)
  - [Databases](#databases)
  - [Tools](#tools)
  - [Design Architecture and Scale](#design-architecture-and-scale)
  - [OtherOps](#otherops)
  - [Projects](#projects)
  - [Tech Companies](#tech-companies)
  - [DevOps/SRE Culture](#devopssre-culture)
    - [Background on DevOps](#background-on-devops)
    - [Background on SRE](#background-on-sre)
  - [Expert Communities](#expert-communities)
    - [Conferences](#conferences)
    - [Local Meetups](#local-meetups)
    - [Podcasts](#podcasts)
    - [Blogs and Newsletters](#blogs-and-newsletters)
    - [Superstars](#superstars)
      - [DevOps Superstars](#devops-superstars)
      - [SRE Superstars](#sre-superstars)

## Basic Operating Systems

### macOS

#### How to capture screenshot directly to clipboard

- `CMD-CTRL-SHIFT-4` for a a cross-hair cursor. This is all I should ever need.

### Linux

#### All Distros

##### cron
- **DO NOT** ever copy any script into `/etc/cron.{daily,weekly,etc}` that has a file extension. If you do. It will never run. Boy did I ever learn that the hard way. Here's how you test, for example, if your scripts in `/etc/cron.weekly` are going to fire:
```
$ run-parts -v /etc/cron.weekly

[output should be]
run-parts: executing /etc/cron.weekly/<YOUR_SCRIPT>
...
```


##### Cpu Information
- Quick and dirty method to get the number of processing units available. Note that the number of processing units might not always be the same as number of cores. (Installed as part of `coreutils` package in Homebrew.)
> ```
> $ nproc
>
> [output on my Macbook]
> 8
> ```

Can also use the following:
- `lscpu`
- `cat /proc/cpuinfo`
- `sudo lshw -class processor`

Third-party (repire additional software installation):
- `cpuid` (super verbose)
- `inxi -C`

#### Ubuntu

- [Ubuntu Bionic: Netplan](https://blog.ubuntu.com/2017/12/01/ubuntu-bionic-netplan)    
  - [Netplan Reference](https://netplan.io/)  
- [How to set DNS nameservers in Ubuntu Server 18.04](https://www.techrepublic.com/article/how-to-set-dns-nameservers-in-ubuntu-server-18-04/)

##### Active Directory / LDAP / SSSD
- Auto Creation of Linux Home Directories (*pam_oddjob_mkhomedir*)
> Found this in Ubuntu official document: [SSSD and Active Directory](https://help.ubuntu.com/lts/serverguide/sssd-ad.html.en)
>
> See section: Home directories with pam_mkhomedir (optional). However, they reference the pam_mkhomedir.so module. We tried this in 2019 (Ubuntu 18.04) and used pam_oddjob_mkhomedir instead. To do so:
> ```
> Make certain oddjob-mkhomedir_x.xx.x-x_amd64.deb is installed
> Make certain that the oddjob service is running
> Modify /etc/pam.d/common-session
>
>   - add this line directly after session required pam_unix.so
>     session    required    pam_oddjob_mkhomedir.so skel=/etc/skel/ umask=0022
>
> This setting might also require override_homedir in sssd.conf to function correctly, so check your work.
> ```

## Basic Storage

### Linux Basic Storage Commands

- What block storage devices are attached?
> ```
> $ lsblk
>
> [output]
> NAME                   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
> sda                      8:0    0   64G  0 disk
> └─sda1                   8:1    0   64G  0 part
>   ├─vagrant--vg-root   253:0    0   63G  0 lvm  /
>   └─vagrant--vg-swap_1 253:1    0  980M  0 lvm  [SWAP]
> ```

- How can I find block device UUIDs?
> ```
> $ sudo blkid
> ```

- What can you use to create and manage physical disk partitions?
> ```
> $ sudo fdisk /dev/<device1>
> ```
> Then type `m` for help and make your way own from there.  
> You can also use `parted`.
> ```
> $ sudo parted /dev/<device1>
>
> [output]
> GNU Parted 3.2
> Using /dev/sda
> Welcome to GNU Parted! Type 'help' to view a list of commands.
> (parted)
> ```
> Type `help` and make your way from there.

#### Logical Volume Manager

- How do you manage and configure LVM Storage?
> Step 1.  
> First thing, you need to make certain that LVM is installed. On a CentOS box, you could do this:
> ```
> $ sudo yum install -y lvm2
> ```
> When you partition your physical devices, you need to ensure that the file type is `Linux LVM` (usually `8e` in fdisk).  
> Step 2.  
> Create physical volumes.  
> ```
> $ sudo pvcreate /dev/<partition1> /dev/<partition2>
> ```  
> Step 3.  
> Create volume groups.  
> ```
> $ sudo vgcreate <volgroup-name> /dev/<partition1> /dev/<partition2>
> ```  
> Step 4.  
> Create a logical volume that belongs to this volume group.  
> ```
> $ sudo lvcreate --name <logvol-name> --size XXG <volgroup-name>
> ```  
> Step 5.  
> Create a file system on your logical volume. (EXT4 example.)
> ```
> $ mkfs -t ext4 /dev/<volgroup-name>/<logvol-name>
> ```  
> Step 6.  
> Mount your new file system. (Example, in /mnt.)
> ```
> $ sudo mkdir /mnt/<mnt-name>
> $ sudo mount /dev/<volgroup-name>/<logvol-name> /mnt/<mnt-name>
> ```
> And don't forget to put a new entry in `/etc/fstab`.
- To scan all of your disks for physical volumes:
> ```
> $ pvscan
> ```
- To display physical volume information:
> ```
> $ sudo pvs
> $ sudo pvdisplay
> ```
- To display volume group information:
> ```
> $ sudo vgs
> $ sudo vgdisplay
> ```
- To display logical volume information:
> ```
> $ sudo lvs
> $ sudo lvdisplay
> ```
- To extend an existing volume group:
> Step 1.  
> You must create a new physical volume.  
> Step 2.  
> You extend the existing volume group as follows:  
> ```
> $ sudo vgextend <volgroup-name> /dev/<partition3>
> ```
> Step 3.  
> You must then extend your logical volume:  
> (Note: run vgdisplay to see the `Free PE/Size value`.)
> ```
> $ sudo lvextend -l +<PE-size> /dev/<volgroup-name>/<logvol-name>
> ```
> Step 4.  
> You must resize the file system. (Ext4 example.)  
> ```
> $ sudo e2fsck -f /dev/<volgroup-name>/<logvol-name>
> $ sudo resize2fs /dev/<volgroup-name>/<logvol-name>
> ```

#### Encrypted Storage

- Does your kernel support encrypted file systems?
> ```
> $ grep -i config_dm_crypt /boot/config-$(uname -r)
>   
> [output]
> CONFIG_DM_CRYPT=m
>  
> $ lsmod | grep dm_crypt
> ```
> Note: it's probably not loaded by default.

- What binaries do you need to encrypt your file system? (Example on CentOS.)
> ```
> $ sudo yum install cryptsetup
> ```

- How do I create an encrypted file system?
> You need to setup an empty partition with no file system. Then you do the following:  
> ```
> $ sudo cryptsetup -y luksFormat /dev/<partition1>
> ```
> You will be prompted to confirm (type `YES`) and you will be prompted to enter and confirm a passphrase.

- How do I unlock the encrypted filesystem?
> ```
> $ sudo cryptsetup luksOpen /dev/<partition1> <fs-name>
> ```
> Note: at the point, the unlocked and encrypted file system is NOT formatted nor is it mounted. You need to do this before you can use it.  

- How do I format and mount the unlocked and encrypted file system? (Ext4 example.)
> ```
> $ sudo mkfs -t ext4 /dev/mapper/<fs-name>
> $ sudo mkdir /mnt/encrypted
> $ sudo mount /dev/mapper/<fs-name> /mnt/encrypted
> ```

- How do I lock the encrypted file system?
> ```
> $ sudo umount /mnt/encrypted
> $ sudo cryptsetup luksClose <fs-name>
> ```
> Not to certain at this point how you accomplish all this during boot...

#### Swap

- How big should swap be?
> Current rule of thumb: 2x RAM for up to 2GB. After that, you can go with a 1:1 ratio. However, this seems excessive to me. In any event, I guess it's dangerous to ever let your free memory dip below 35MB. [I need more sources to verify this.]

- How do I turn swap off/on? (Some applications require this...)
> ```
> $ sudo swapoff -a
> $ sudo swapon -a
> ```

## Basic Networking

## Basic Monitoring

## Basic Security

### SSH

- [SSH Hardening Guides](https://www.sshaudit.com/hardening_guides.html): Guides for hardening SSH for systems running Ubuntu, RHEL/CentOS, pfSense, and OpenBSD. Mostly involves regenerating host keys, removing small Diffie-Hellman moduli, disabling DSA and ECDSA host keys, and restricting supported key exchange, cipher, and MAC algorithms.  

- [OpenSSH moduli](https://entropux.net/article/openssh-moduli/): The /etc/ssh/moduli file contains prime numbers and generators for use by sshd(8) in the Diffie-Hellman Group Exchange key exchange method. It's used in the beginning of SSH sessions to generate a shared secret between the client and the server. Each distro provides a pregenerated moduli file that includes 1024 - 8096 bit modulus. You could generate your own but it would take ~4h to generate prime candidates for just 4096 bit modulus. Noted. Even so, here's the method for generating a 4096 bit modulus file.

> **Step 1**: First, generate "candidate primes":
> ```
> ssh-keygen -G /tmp/4096.1 -b 4096
> ```
>
> That should create ~10,000 candidates.  
>
> **Step 2**: Run it a second time using the first output file as input so that ~30 safe and trusted candidates are preserved (this will take a while):
> ```
> ssh-keygen -T /tmp/4096.2 -f /tmp/4096.1
> ```
>
> You can now use the second output file as a ready-to-use moduli file.

- [How To: Ubuntu / Debian Linux Regenerate OpenSSH Host Keys](https://www.cyberciti.biz/faq/howto-regenerate-openssh-host-keys/): Method for regenerating SSH Host keys for Debian/Ubuntu instances using the `dpkg-reconfigure` command. Quickstart:

> **Step 1**: As root, remove the existing SSH Host keys.
> ```
> rm -fv /etc/ssh/ssh_host_*
>
> [output]
> removed '/etc/ssh/ssh_host_dsa_key'
> removed '/etc/ssh/ssh_host_dsa_key.pub'
> removed '/etc/ssh/ssh_host_ecdsa_key'
> removed '/etc/ssh/ssh_host_ecdsa_key.pub'
> removed '/etc/ssh/ssh_host_ed25519_key'
> removed '/etc/ssh/ssh_host_ed25519_key.pub'
> removed '/etc/ssh/ssh_host_rsa_key'
> removed '/etc/ssh/ssh_host_rsa_key.pub'
> ```
> **Step 2**: Regenerate OpenSSH Host keys:
> ```
> dpkg-reconfigure openssh-server
>
> [output]
> Creating SSH2 RSA key; this may take some time ...
> and so on...
> ```
> Step 3: Restart SSH:
> ```
> systemctl restart ssh
> ```


## Cloud

### AWS

#### EC2

- Disable instance termination protection
```
aws ec2 modify-instance-attribute --instance-id <instance-ID> --no-disable-api-termination
```
 - Terminate the instance
```
aws ec2 terminate-instances --instance-ids <instance-ID>
```

> You can combine the two commands into a very basic script that looks something like this:
>```
>#!/usr/bin/env bash
>
> # instances to be deleted
> instance_ids=(
>     i-07ae4f5b766xxxxxx
>     i-0a5fec63344xxxxxy
> )
>
> # disable termination protection
> for id in "${instance_ids[@]}"
> do
>     aws ec2 modify-instance-attribute --instance-id "${id}" --no-disable-api-termination
> done
>
> # terminate the instances
> for id in "${instance_ids[@]}"
> do
>     aws ec2 terminate-instances --instance-ids "${id}"
> done
>```

## Containers

## Orchestration

## Automation

## Programming

### Code Snippets

- [snippet generator](https://snippet-generator.app/) (VS, Sublime, Atom)

#### VS Code

- [VS Code snippets: the most powerful tool to boost your coding productivity](https://medium.freecodecamp.org/the-most-powerful-tool-to-boost-your-coding-productivity-2dc80e0eff00)


### Python

- [Talk Python Training](https://training.talkpython.fm/): Online courses for Python developers.

### Golang

- [Gophercises](https://gophercises.com/): Free courses that offer instructions for building roughly 20 different mini-applications, packages, and tools.
- [The Complete Guide to Learning Go](https://www.calhoun.io/guide-to-go/): Joh Calhoun's curated of a billion Go learning resources spread throughout the Web. Great starting point for individuals who are new to Golang. Provides links to Jon's paid courses.

#### Golang: Atom Configuration

- Install package: [go-plus](https://atom.io/packages/go-plus)  
  - You will be prompted to enable various features.  
  - Selecte `Golang: Update Tools` to update at any time.    
- While your at it, install the [Atom Material UI](https://atom.io/themes/atom-material-ui) theme and the [Oceanic One Dark Syntax](https://atom.io/themes/oceanic-one-dark-syntax) theme.

## Applications

### OmniGraffle

#### How to install stencils (Mac)

- Download from the stencil from the Internet
- Double-click to unzip the package
- Double-click to 'move' the stencil to your local Stencil Library
> Where is the local Stencil Library located?
> > Who even knows?  

> Where can you get more stencils?
> > - [Stenciltown](https://stenciltown.omnigroup.com/categories/all/)  
> > - [Graffletopia](https://www.graffletopia.com/)  

### vim

- [Moving around](https://vim.fandom.com/wiki/Moving_around)

> Most useful commands are:
> ```
> Start of a line      --> 0
> End of a line        --> $
> Top of doc           --> gg or 1G
> Bottom of doc        --> G
> Top of the screen    --> H
> Middle of the screen --> M
> Bottom of the screen --> L
> Forward 1 line       --> j
> 10 lines forward     --> 10j
> 10 lines backwards   --> 10k
> Jump to line 50      --> 50G
> Forward 1 sentence   --> )
> Backward 1 sentence  --> (
> Forward 1 paragraph  --> }
> Backward 1 paragraph --> {
> ```

- [Undo and Redo](https://vim.fandom.com/wiki/Undo_and_Redo)

> ```
> Undo last change --> u
> Undo the undos   --> Ctrl-r
>
> Careful with this one!
> Return last line which was modified to it's original state --> U
> ```

- Global search and replace

> ```
> :%s/search_string/replacement_string/g
> ```

## Databases

## Tools

## Design Architecture and Scale

## OtherOps

## Projects

## Tech Companies

## DevOps/SRE Culture

> *class SRE implements interface DevOps*

### Background on DevOps
Most of this from: [The Site Reliability Workbook](http://www.allitebooks.in/the-site-reliability-workbook/)

- loose set of practices, guidelines, and culture designed to break down silos in IT development, operations, networking, and security.
- originators: John Willis, Damon Edwards, and Jez Humble
- **CA(L)MS**:
  - **Culture** (end result),
  - **Automation** (make improvements),
  - **Lean** (manufacturing - continuous deployment),
  - **Measurement** (results of improvement),
  - **Sharing** (collaberation)
- key tenents:
  - no more silos (dev and ops on the same team)
  - accidents are normal (result from missing safeguards for when things inevitably go wrong)
  - change should be gradual (change is best when it's small and frequent)
  - tooling and culture are interrelated (a good culture can work around broken tooling but change management relies on highly specific tools)
  - measurement is crucial (particulary in the overall business context of breaking down silos and incident resolution)

### Background on SRE
Most of this from: [The Site Reliability Workbook](http://www.allitebooks.in/the-site-reliability-workbook/)

- term coined by Ben Treynor Sloss (VP of engineering at Google)
- job role and set of practices that seem to work for Google and maybe other organizations as well:
    - operations is a software problem (use software engineering approaches to solve problems)
    - manage services by SLO (service level objectives - no such thing as 100% availability)
    - work to minimize toil (any manual or structurally mandated task is abhorrent - make the machines do *all* the labor)
    - automate this year's job away (hard limit on how much time to spend on toil vs engineering)
      - **Murphy-Beyer effect** (wind up automating all that you can for a service, leaving behind things that can't be automated)
      - what to do with all the extra time? Create more services...
    - move fast by reducing the cost of failure (starts by improving late problem discovery - reduce mean time to repair **MTTR**)
    - share ownership with developers (no rigid boundary between AppDev and PROD)
      - required expertise (bread-and-butter competencies) around availablity, latency, perfermance, efficiency, change management, monitoring, emergency response, and capacity planning
    - use the same tooling, regardless of function or job title (teams minding a service should use the same tools, regardless of their roles in the org)

## Expert Communities

### Conferences

### Local Meetups

- [DevOps Vancouver BC Canada](https://www.meetup.com/DevOps-Vancouver-BC-Canada/)


### Podcasts

### Blogs and Newsletters

### Superstars

#### DevOps Superstars
- [John Willis](https://twitter.com/botchagalupe)
- [Damon Edwards](https://twitter.com/damonedwards)
- [Jez Humble](https://twitter.com/jezhumble)
- [Gene Kim](https://twitter.com/RealGeneKim)

#### SRE Superstars
- [Ben Treynor Sloss](https://www.linkedin.com/in/benjamin-treynor-sloss-207120/)
